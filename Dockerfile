FROM traefik:alpine

RUN adduser traefik -D ; \
  apk add --no-cache libcap; \
  chmod u+s $(which setcap); \
  sed '2asetcap cap_net_bind_service=+eip $(which traefik)' -i /entrypoint.sh; \
  chown traefik: /entrypoint.sh $(which traefik)

USER "traefik"

EXPOSE 80 8080

ENTRYPOINT ["/entrypoint.sh", "--web.address=:8080"]
